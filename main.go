package main

import (
	"encoding/json"
	"net/http"
)

func main() {
	http.HandleFunc("/api", helloHandler)
	http.ListenAndServe(":5000", nil)
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	json, _ := json.Marshal("Hello World!")
	w.Write(json)
}