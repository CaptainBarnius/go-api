FROM golang:1.11-alpine3.7
RUN apk add --no-cache \
  openssh-client \
  ca-certificates \
  bash